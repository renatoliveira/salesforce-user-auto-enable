# User Auto Enablement

Use this package to automatically enable selected users on a sandbox refresh.

## Usage

Specify the `UserAutoEnablement` class as the post install script when creating or refreshing a sanbox instance, or call the methods that execute its code from your own post install script.

Affected users are the ones with the "Enable on Sandbox Refresh" field checked. To make this field appear on your user layout's page, add it there first as this app does not provide it on the layout.
