/**
 * Run this class' execute method to activate the selected users automatically.
 *
 * Alternativaly, if you already have a class implementing the SandboxPostCopy
 * you could just call UserAutoEnablement.execute after it does its thing.
 */
public without sharing class UserAutoEnablement implements SandboxPostCopy, Queueable {
    @SuppressWarnings('PMD.ApexCRUDViolation')
    public static void execute() {
        List<User> usersToEnable = [
            SELECT Id, Email
            FROM User
            WHERE EnableOnPostCopy__c = TRUE AND IsActive = TRUE
        ];

        for (User u : usersToEnable) {
            if (u.Email.endsWith('invalid')) {
                u.Email = u.Email.removeEnd('.invalid');
            }
        }

        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.EmailHeader.TriggerUserEmail = true;
        Database.update(usersToEnable, dmlOptions);
    }

    public static void executeAsync() {
        System.enqueueJob(new UserAutoEnablement());
    }

    public void runApexClass(SandboxContext context) {
        execute();
    }

    public void execute(QueueableContext context) {
        runApexClass(null);
    }
}
