@IsTest
private class UserAutoEnablementTest {
    @IsTest
    static void test() {
        // Test data setup
        User u = new User(Id = UserInfo.getUserId());
        u.EnableOnPostCopy__c = true;
        u.Email = UserInfo.getUserEmail() + '.invalid';
        update u;

        // Actual test
        Test.startTest();
        UserAutoEnablement.executeAsync();
        Test.stopTest();

        // Asserts
        u = [SELECT Id, Email FROM User WHERE Id = :UserInfo.getUserId()];

        System.assert(
            !u.Email.contains('.invalid'),
            'Should not contain "invalid" at the end of email.'
        );
    }
}
